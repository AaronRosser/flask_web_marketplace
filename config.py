# Production
class DefaultConfig(object):
    DEBUG = False
    Testing = False
    SECRET_KEY = '009331911b26799fe4b894bc3b3c4fc1e86883b94191ed6ef0dbdae1a53af99a'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///site.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SAFE_URL_HOSTS = {'sc19ar.pythonanywhere.com'}


class DevolpmentConfig(DefaultConfig):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///site.db'
    SAFE_URL_HOSTS = {'127.0.0.1:5000'}


class ProductionConfig(DefaultConfig):
    pass


class TestConfig(DefaultConfig):
    TESTING = True
    WTF_CSRF_ENABLED = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///:memory:'
    SAFE_URL_HOSTS = {'127.0.0.1:5000'}
