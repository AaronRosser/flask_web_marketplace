from flask_api import status

from Online_Store import db
from Online_Store.models import Review

import unittest
from tests.base import BaseTestCase
from sqlalchemy import func


class ReviewTestCase(BaseTestCase):
    def get_number_reviews(self):
        return db.session.query(func.count(Review.id)).scalar()

    # Post review
    def test_review_post_correct(self):
        self.login_u1()
        num_reviews = self.get_number_reviews()
        with self.client:
            params = dict(
                content='New review 1234',
                rating=3,
                csrf_token=self.csrf_token
            )
            res = self.client.post('/product/1/review/post', data=params, follow_redirects=True)
            self.assert200(res)

            # Check review was created and displayed
            self.assertEqual(self.get_number_reviews(), num_reviews + 1)
            r = Review.query.filter(
                Review.author_id == 1,
                Review.product_id == 1,
                Review.content == 'New review 1234',
                Review.rating == 3
            ).first()
            self.assertIsNotNone(r)
            self.assertReview(r, res.data, view_product=False)

    def test_review_post_invalid_product(self):
        self.login_u1()
        num_reviews = self.get_number_reviews()
        with self.client:
            params = dict(
                content='New review 1234',
                rating=3,
                csrf_token=self.csrf_token
            )
            res = self.client.post('/product/999/review/post', data=params, follow_redirects=True)
            self.assertIn(b'Product with id [999] not found', res.data)

            # Check review was not created
            self.assertEqual(self.get_number_reviews(), num_reviews)
            r = Review.query.filter(
                Review.product_id == 999,
            ).first()
            self.assertIsNone(r)

    def test_review_post_invalid_form(self):
        self.login_u1()
        num_reviews = self.get_number_reviews()
        with self.client:
            params = dict(
                content='Short',
                rating=999,
                csrf_token=self.csrf_token
            )
            res = self.client.post('/product/1/review/post', data=params, follow_redirects=True)
            self.assert400(res)
            self.assertIn(b'Field must be between 10 and 120 characters long', res.data)
            self.assertIn(b'Ratings must be between 1 and 5 stars', res.data)

            # Check review was not created
            self.assertEqual(self.get_number_reviews(), num_reviews)
            r = Review.query.filter(
                Review.rating == 999,
            ).first()
            self.assertIsNone(r)

    def test_review_post_not_logged_in(self):
        num_reviews = self.get_number_reviews()
        with self.client:
            params = dict(
                content='New review 1234',
                rating=3,
                csrf_token=self.csrf_token
            )
            res = self.client.post('/product/1/review/post', data=params)
            self.assertRedirects(res, 'user/login?next=%2Fproduct%2F1%2Freview%2Fpost')

            # Check review was not created
            self.assertEqual(self.get_number_reviews(), num_reviews)

    # Delete review
    def test_delete_review_logged_in_seller(self):
        self.login_u1()
        num_reviews = self.get_number_reviews()
        with self.client:
            params = dict(
                id=1,
                csrf_token=self.csrf_token
            )
            res = self.client.post('/review/delete', data=params)
            self.assert200(res)
            self.assertJsonResponse(res, 'Successfully deleted review with id [1]', status.HTTP_200_OK)

            # Check review was deleted
            self.assertEqual(self.get_number_reviews(), num_reviews - 1)
            p = Review.query.get(1)
            self.assertIsNone(p)

    def test_delete_review_logged_in_not_seller(self):
        self.login_u2()
        num_reviews = self.get_number_reviews()
        with self.client:
            params = dict(
                id=1,
                csrf_token=self.csrf_token
            )
            res = self.client.post('/review/delete', data=params)
            self.assert400(res)
            self.assertJsonResponse(res, 'Review with id [1] does not belong to current user',
                                    status.HTTP_400_BAD_REQUEST)

            # Check review was not deleted
            self.assertEqual(self.get_number_reviews(), num_reviews)
            r = Review.query.get(1)
            self.assertIsNotNone(r)
            self.assertEqual(r.content, 'Good banana')

    def test_delete_review_not_logged_in(self):
        num_reviews = self.get_number_reviews()
        with self.client:
            params = dict(
                id=1,
                csrf_token=self.csrf_token
            )
            res = self.client.post('/review/delete', data=params)
            self.assert401(res)
            self.assertJsonResponse(res, 'Unauthorized', status.HTTP_401_UNAUTHORIZED)

            # Check review was not deleted
            self.assertEqual(self.get_number_reviews(), num_reviews)
            r = Review.query.get(1)
            self.assertIsNotNone(r)
            self.assertEqual(r.content, 'Good banana')

    def test_delete_review_invalid(self):
        self.login_u1()
        num_reviews = self.get_number_reviews()
        with self.client:
            self.login_u1()
            params = dict(
                id=999,
                csrf_token=self.csrf_token
            )
            res = self.client.post('/review/delete', data=params)
            self.assert400(res)
            self.assertJsonResponse(res, 'Review with id [999] not found',
                                    status.HTTP_400_BAD_REQUEST)
            self.assertEqual(self.get_number_reviews(), num_reviews)

    def test_delete_review_missing_id(self):
        self.login_u1()
        num_reviews = self.get_number_reviews()
        with self.client:
            params = dict(
                csrf_token=self.csrf_token
            )
            res = self.client.post('/review/delete', data=params)
            self.assert400(res)
            self.assertJsonResponse(res, 'Missing review id', status.HTTP_400_BAD_REQUEST)
            self.assertEqual(self.get_number_reviews(), num_reviews)


if __name__ == '__main__':
    unittest.main()
