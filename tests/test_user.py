from Online_Store import app, db
from Online_Store.models import User, Product, Review

import unittest
from tests.base import BaseTestCase


class UserTestCase(BaseTestCase):
    # Login
    def test_login_loads(self):
        with self.client:
            res = self.client.get('/user/login')
            self.assert200(res)
            self.assertIn(b'Login', res.data)

    def test_login_correct(self):
        with self.client:
            res = self.login_u1()
            self.assert200(res)
            self.assertIn(b'Login successful', res.data)

    def test_login_correct_redirect(self):
        with self.client:
            params = dict(
                email='u1@u1.com',
                password='password1',
                csrf_token=self.csrf_token
            )
            res = self.client.post('/user/login?next=%2Fuser%2Fmanage', data=params)
            self.assertRedirects(res, '/user/manage')

    def test_login_redirect_when_logged_in(self):
        with self.client:
            self.login_u1()
            res = self.client.post('/user/login?next=%2Fuser%2Fmanage')
            self.assertRedirects(res, '/user/manage')

    def test_login_invalid_email(self):
        with self.client:
            res = self.login('not an email', 'password1')
            self.assert400(res)
            self.assertIn(b'Invalid email address', res.data)

    def test_login_not_registered_email(self):
        with self.client:
            res = self.login('not.registered@u1.com', 'password1')
            self.assert400(res)
            self.assertIn(b'Login failed', res.data)

    def test_login_incorrect_password(self):
        with self.client:
            res = self.login('u1@u1.com', 'wrong password')
            self.assert400(res)
            self.assertIn(b'Login failed', res.data)

    # Logout
    def test_logout_correct(self):
        with self.client:
            self.login_u1()
            res = self.logout()
            self.assert200(res)
            self.assertIn(b'Logout successful', res.data)

    def test_logout_csrf_fail(self):
        with self.client:
            self.login_u1()
            res = self.client.post('/user/logout', data={}, follow_redirects=True)
            self.assertIn(b'Logout failed', res.data)

    def test_logout_not_logged_in(self):
        with self.client:
            res = self.logout()
            self.assertIn(b'Logout failed', res.data)

    # Register
    def test_register_loads(self):
        with self.client:
            res = self.client.get('/user/register')
            self.assert200(res)
            self.assertIn(b'Register', res.data)

    def test_register_redirect_when_logged_in(self):
        with self.client:
            self.login_u1()
            res = self.client.post('/user/register')
            self.assertRedirects(res, '/')

    # Test causes werkzeug\test.py:1016: ResourceWarning: unclosed file <_io.BufferedRandom name=4>
    # Because of picture upload
    def test_register_correct(self):
        with self.client:
            params = dict(
                name='new name',
                username='new username',
                email='email@example.com',
                password='password',
                password_confirm='password',
                picture=self.get_picture_parameter(),
                csrf_token=self.csrf_token
            )
            res = self.client.post('/user/register', data=params, content_type='multipart/form-data',
                                   follow_redirects=True)

            self.assert200(res)
            self.assertIn(b'User [ new username ] was created successfully', res.data)

            u = User.query.filter_by(
                email='email@example.com'
            ).first()
            self.assertEqual(u.name, 'new name')
            self.assertEqual(u.username, 'new username')
            self.assertIsNotNone(u.password_hash)
            self.assertIsNotNone(u.picture_name)

    # Causes flask\testing.py:244: ResourceWarning: unclosed file <_io.BufferedRandom name=3>
    # Because of picture upload
    def test_register_incorrect_dup_details(self):
        with self.client:
            params = dict(
                name='new name',
                username='U1 username',
                email='u1@u1.com',
                password='password',
                password_confirm='password',
                picture=self.get_picture_parameter(),
                csrf_token=self.csrf_token
            )
            res = self.client.post('/user/register', data=params,
                                   content_type='multipart/form-data', follow_redirects=True)

            self.assert400(res)
            self.assertIn(b'Username already registered to an account', res.data)
            self.assertIn(b'Email already registered to an account', res.data)

            users = User.query.filter_by(
                email='u1@u1.com'
            ).all()
            self.assertTrue(len(users) == 1)

    def test_register_incorrect_invalid_form(self):
        with self.client:
            params = dict(
                name='a',
                username='very long username greater than 25 characters',
                email='not an email',
                password='different passwords',
                password_confirm='different passwords 123',
                picture='',
                csrf_token=self.csrf_token
            )
            res = self.client.post('/user/register', data=params,
                                   content_type='multipart/form-data', follow_redirects=True)
            self.assert400(res)
            self.assertIn(b'Field must be between 2 and 25 characters long', res.data)
            self.assertIn(b'Invalid email address', res.data)
            self.assertIn(b'Field must be equal to password', res.data)
            self.assertIn(b'Profile picture is required', res.data)

            u = User.query.filter_by(
                email='not an email'
            ).first()
            self.assertIsNone(u)

    # User profile
    def test_user_profile_loads(self):
        with self.client:
            res = self.client.get('/user/1')
            self.assert200(res)
            self.assertIn(b'U1 username&#39;s Profile', res.data)
            self.assertIn(b'U1 name', res.data)

    def test_user_profile_invalid_user(self):
        with self.client:
            res = self.client.get('/user/999', follow_redirects=True)
            self.assertIn(b'User with id [999] not found', res.data)

    # Manage user
    def test_manage_user_redirects_not_logged_in(self):
        with self.client:
            res = self.client.get('/user/manage')
            self.assertRedirects(res, 'user/login?next=%2Fuser%2Fmanage')

    def test_manage_user_loads_logged_in(self):
        with self.client:
            self.login_u1()
            res = self.client.get('/user/manage')
            self.assert200(res)
            self.assertIn(b'Manage Account', res.data)
            self.assertIn(b'Update Details', res.data)
            self.assertIn(b'Change Password', res.data)

    # Manage user - update details
    def test_change_user_details_correct_same(self):
        with self.client:
            self.login_u1()
            params = dict(
                name='U1 name',
                username='U1 username',
                email='u1@u1.com',
                picture='',
                csrf_token=self.csrf_token
            )
            res = self.client.post('/user/manage/update_user_details', data=params)
            self.assert200(res)
            self.assertIn(b'Manage Account', res.data)
            self.assertIn(b'Details successfully updated', res.data)

            u = User.query.get(1)
            self.assertEqual(params['name'], u.name)
            self.assertEqual(params['username'], u.username)
            self.assertEqual(params['email'], u.email)

    def test_change_user_details_correct_changed(self):
        with self.client:
            self.login_u1()
            params = dict(
                name='new name',
                username='new username',
                email='u1@example.com',
                picture='',
                csrf_token=self.csrf_token
            )
            res = self.client.post('/user/manage/update_user_details', data=params)
            self.assert200(res)
            self.assertIn(b'Manage Account', res.data)
            self.assertIn(b'Details successfully updated', res.data)

            u = User.query.get(1)
            self.assertEqual(params['name'], u.name)
            self.assertEqual(params['username'], u.username)
            self.assertEqual(params['email'], u.email)

    # Causes flask\testing.py:244: ResourceWarning: unclosed file <_io.BufferedRandom name=3>
    # Because of picture upload
    def test_change_user_details_new_picture(self):
        with self.client:
            self.login_u1()
            old_picture_name = User.query.get(1).picture_name

            params = dict(
                name='U1 name',
                username='U1 username',
                email='u1@u1.com',
                picture=self.get_picture_parameter(),
                csrf_token=self.csrf_token
            )
            res = self.client.post('/user/manage/update_user_details', data=params)
            self.assert200(res)
            self.assertIn(b'Manage Account', res.data)
            self.assertIn(b'Details successfully updated', res.data)

            # Check picture name was updated
            u = User.query.get(1)
            self.assertNotEqual(old_picture_name, u.picture_name)

    def test_change_user_details_invalid(self):
        with self.client:
            self.login_u1()
            params = dict(
                name='a',
                username='very long username greater than 25 characters',
                email='not an email',
                picture='',
                csrf_token=self.csrf_token
            )
            res = self.client.post('/user/manage/update_user_details', data=params)
            self.assert400(res)
            self.assertIn(b'Manage Account', res.data)
            self.assertIn(b'Field must be between 2 and 25 characters long', res.data)
            self.assertIn(b'Invalid email address', res.data)

    def test_change_user_details_duplicate(self):
        with self.client:
            self.login_u1()
            params = dict(
                name='new name',
                username='U2 username',
                email='u2@u2.com',
                picture='',
                csrf_token=self.csrf_token
            )
            res = self.client.post('/user/manage/update_user_details', data=params)
            self.assert400(res)
            self.assertIn(b'Manage Account', res.data)
            self.assertIn(b'Username already registered to an account', res.data)
            self.assertIn(b'Email already registered to an account', res.data)

    # Manage user - change password
    def test_user_change_password_correct_same(self):
        with self.client:
            self.login_u1()
            old_password_hash = User.query.get(1).password_hash

            params = dict(
                current_password='password1',
                password='password1',
                password_confirm='password1',
                csrf_token=self.csrf_token
            )
            res = self.client.post('/user/manage/change_password', data=params)
            self.assert200(res)
            self.assertIn(b'Manage Account', res.data)
            self.assertIn(b'Password updated successfully', res.data)

            # Ensure password was changed
            u = User.query.get(1)
            self.assertNotEqual(old_password_hash, u.password_hash)

    def test_user_change_password_correct(self):
        with self.client:
            self.login_u1()
            old_password_hash = User.query.get(1).password_hash

            params = dict(
                current_password='password1',
                password='password2',
                password_confirm='password2',
                csrf_token=self.csrf_token
            )
            res = self.client.post('/user/manage/change_password', data=params)
            self.assert200(res)
            self.assertIn(b'Manage Account', res.data)
            self.assertIn(b'Password updated successfully', res.data)

            # Ensure password was changed
            u = User.query.get(1)
            self.assertNotEqual(old_password_hash, u.password_hash)

    def test_user_change_password_incorrect_password(self):
        with self.client:
            self.login_u1()
            old_password_hash = User.query.get(1).password_hash

            params = dict(
                current_password='wrong password',
                password='password2',
                password_confirm='password2',
                csrf_token=self.csrf_token
            )
            res = self.client.post('/user/manage/change_password', data=params)
            self.assert400(res)
            self.assertIn(b'Manage Account', res.data)
            self.assertIn(b'Incorrect password', res.data)

            # Ensure password was not changed
            u = User.query.get(1)
            self.assertEqual(old_password_hash, u.password_hash)

    def test_user_change_password_invalid_form(self):
        with self.client:
            self.login_u1()
            params = dict(
                current_password='password1',
                password='this does not match',
                password_confirm='password2',
                csrf_token=self.csrf_token
            )
            res = self.client.post('/user/manage/change_password', data=params)
            self.assert400(res)
            self.assertIn(b'Manage Account', res.data)
            self.assertIn(b'Field must be equal to password', res.data)

    # Listings
    def test_user_listings_correct(self):
        with self.client:
            res = self.client.get('/user/1/listings')
            self.assert200(res)
            self.assertIn(b'U1 username&#39;s listings', res.data)
            u = User.query.get(1)
            for p in u.products:
                self.assertProductPreview(p, res.data)

    def test_user_listings_no_products(self):
        with self.client:
            # Delete all products for u1
            products = Product.query.filter(
                Product.seller_id == 1
            ).all()
            for p in products:
                db.session.delete(p)
            db.session.commit()

            res = self.client.get('/user/1/listings')
            self.assert200(res)
            self.assertIn(b'U1 username&#39;s listings', res.data)
            self.assertIn(b'No products found', res.data)

    def test_user_listings_invalid_user(self):
        with self.client:
            res = self.client.get('/user/999/listings', follow_redirects=True)
            self.assertIn(b'User with id [999] not found', res.data)

    # Reviews
    def test_user_reviews_correct(self):
        with self.client:
            res = self.client.get('/user/1/reviews')
            self.assert200(res)
            self.assertIn(b'U1 username&#39;s reviews', res.data)
            u = User.query.get(1)
            for r in u.reviews:
                self.assertReview(r, res.data)

    def test_user_reviews_no_reviews(self):
        with self.client:
            # Delete all reviews for u1
            reviews = Review.query.filter(
                Review.author_id == 1
            ).all()
            for r in reviews:
                db.session.delete(r)
            db.session.commit()

            res = self.client.get('/user/1/reviews')
            self.assert200(res)
            self.assertIn(b'U1 username&#39;s reviews', res.data)
            self.assertIn(b'No reviews found', res.data)

    def test_user_reviews_invalid_user(self):
        with self.client:
            res = self.client.get('/user/999/reviews', follow_redirects=True)
            self.assertIn(b'User with id [999] not found', res.data)


if __name__ == '__main__':
    unittest.main()
