from flask_api import status

from Online_Store import db
from Online_Store.models import User, Product

import unittest
from tests.base import BaseTestCase


class WishlistTestCase(BaseTestCase):
    # Add product to wishlist
    def test_wishlist_add_correct(self):
        with self.client:
            self.login_u1()
            params = dict(
                id=1,
                csrf_token=self.csrf_token
            )
            res = self.client.post('/wishlist/add', data=params)
            self.assert200(res)
            self.assertJsonResponse(res, 'Product successfully added to wishlist', status.HTTP_200_OK)

            # Check relationship added to database
            u = User.query.get(1)
            p = Product.query.get(1)
            self.assertTrue(len(u.wishlist_products) == 1)
            self.assertIn(p, u.wishlist_products)

    def test_wishlist_add_twice(self):
        with self.client:
            self.login_u1()
            params = dict(
                id=1,
                csrf_token=self.csrf_token
            )
            self.client.post('/wishlist/add', data=params)
            res = self.client.post('/wishlist/add', data=params)
            self.assert400(res)
            self.assertJsonResponse(res, 'Product already in wishlist', status.HTTP_400_BAD_REQUEST)

            # Check relationship was not added to database twice
            u = User.query.get(1)
            p = Product.query.get(1)
            self.assertTrue(len(u.wishlist_products) == 1)
            self.assertIn(p, u.wishlist_products)

    def test_wishlist_add_invalid_product(self):
        with self.client:
            self.login_u1()
            params = dict(
                id=999,
                csrf_token=self.csrf_token
            )
            res = self.client.post('/wishlist/add', data=params)
            self.assert400(res)
            self.assertJsonResponse(res, 'Product not found', status.HTTP_400_BAD_REQUEST)

            # Check relationship was not added to database
            u = User.query.get(1)
            self.assertTrue(len(u.wishlist_products) == 0)

    def test_wishlist_add_missing_product_id(self):
        with self.client:
            self.login_u1()
            res = self.client.post('/wishlist/add')
            self.assert400(res)
            self.assertJsonResponse(res, 'Invalid or missing product id', status.HTTP_400_BAD_REQUEST)

            # Check relationship was not added to database
            u = User.query.get(1)
            self.assertTrue(len(u.wishlist_products) == 0)

    def test_wishlist_add_not_logged_in(self):
        with self.client:
            params = dict(
                id=1,
                csrf_token=self.csrf_token
            )
            res = self.client.post('/wishlist/add', data=params)
            self.assert401(res)
            self.assertJsonResponse(res, 'Unauthorized', status.HTTP_401_UNAUTHORIZED)

    # Remove product from wishlist
    def test_wishlist_remove_correct(self):
        with self.client:
            self.login_u2()
            params = dict(
                id=1,
                csrf_token=self.csrf_token
            )
            res = self.client.post('/wishlist/remove', data=params)
            self.assert200(res)
            self.assertJsonResponse(res, 'Product successfully removed from wishlist', status.HTTP_200_OK)

            # Check product no longer in wishlist
            u = User.query.get(2)
            p = Product.query.get(1)
            self.assertNotIn(p, u.wishlist_products)

    def test_wishlist_remove_twice(self):
        with self.client:
            self.login_u2()
            params = dict(
                id=1,
                csrf_token=self.csrf_token
            )
            self.client.post('/wishlist/remove', data=params)
            res = self.client.post('/wishlist/remove', data=params)
            self.assert400(res)
            self.assertJsonResponse(res, 'Product not in wishlist', status.HTTP_400_BAD_REQUEST)

            # Check product no longer in wishlist
            u = User.query.get(2)
            p = Product.query.get(1)
            self.assertNotIn(p, u.wishlist_products)

    def test_wishlist_remove_invalid_product(self):
        with self.client:
            self.login_u2()
            params = dict(
                id=999,
                csrf_token=self.csrf_token
            )
            res = self.client.post('/wishlist/remove', data=params)
            self.assert400(res)
            self.assertJsonResponse(res, 'Product not found', status.HTTP_400_BAD_REQUEST)

    def test_wishlist_remove_missing_product_id(self):
        with self.client:
            self.login_u2()
            res = self.client.post('/wishlist/remove')
            self.assert400(res)
            self.assertJsonResponse(res, 'Invalid or missing product id', status.HTTP_400_BAD_REQUEST)

    def test_wishlist_remove_not_logged_in(self):
        with self.client:
            params = dict(
                id=1,
                csrf_token=self.csrf_token
            )
            res = self.client.post('/wishlist/remove', data=params)
            self.assert401(res)
            self.assertJsonResponse(res, 'Unauthorized', status.HTTP_401_UNAUTHORIZED)

    # Wishlist page
    def test_wishlist_correct(self):
        with self.client:
            self.login_u2()
            res = self.client.get('/wishlist')
            self.assert200(res)
            self.assertIn(b'My wishlist', res.data)
            u = User.query.get(2)
            for p in u.wishlist_products:
                self.assertProductPreview(p, res.data)

    def test_wishlist_no_products(self):
        self.login_u2()
        # Remove all u2's wishlisted products
        u2 = User.query.get(2)
        u2.wishlist_products.clear()
        db.session.commit()

        with self.client:
            res = self.client.get('/wishlist')
            self.assert200(res)
            self.assertIn(b'My wishlist', res.data)
            self.assertIn(b'No products found', res.data)

    def test_wishlist_not_logged_in(self):
        with self.client:
            res = self.client.get('/wishlist')
            self.assertRedirects(res, '/user/login?next=%2Fwishlist')


if __name__ == '__main__':
    unittest.main()
