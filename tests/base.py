import os

from flask import url_for
from flask_testing import TestCase

from Online_Store import app, db, SimpleForm
from Online_Store.models import User, Product, Review

import warnings
from io import BytesIO
import json


class BaseTestCase(TestCase):
    def create_app(self):
        app.config.from_object('config.TestConfig')
        return app

    def setUp(self):
        # Ignore warnings from testing picture uploads
        warnings.simplefilter('ignore', category=ResourceWarning)
        # Ignore warning from escaped . in regex
        warnings.simplefilter('ignore', category=DeprecationWarning)

        # Create in memory database
        db.create_all()

        # Create some example users
        u1 = User(
            name='U1 name',
            username='U1 username',
            email='u1@u1.com',
            picture_name=''
        )
        u1.set_password('password1')
        db.session.add(u1)

        u2 = User(
            name='U2 name',
            username='U2 username',
            email='u2@u2.com',
            picture_name=''
        )
        u2.set_password('password2')
        db.session.add(u2)

        db.session.commit()

        # Add some example products
        p1 = Product(
            name='Banana',
            description='U1s banana',
            price=1.56,
            currency='£ (GBP)',
            ebay_link='https://www.ebay.co.uk/product345',
            picture_name='425c794c15b84d8cd844c960.jpg',
            amazon_link='https://www.amazon.co.uk/product345',
            category=5
        )
        u1.products.append(p1)

        p2 = Product(
            name='Apple',
            description='U1 apple',
            price=2.65,
            currency='$ (USD)',
            ebay_link='https://www.ebay.co.uk/product567',
            picture_name='e52e5ca03369e9d282bea813.jpg',
            amazon_link='',
            category=5
        )
        u1.products.append(p2)

        p3 = Product(
            name='Orange',
            description='U1 orange',
            price=1.2,
            currency='$ (AUD)',
            ebay_link='https://www.ebay.co.uk/product587438',
            picture_name='fd4e4dfb7dea4ed0f99d2b61.jpg',
            amazon_link='',
            category=5
        )
        u1.products.append(p3)

        p4 = Product(
            name="Lemon",
            description="U2 lemon",
            price=2.5,
            currency="£ (GBP)",
            ebay_link="",
            picture_name="23be665d6a00a7ca6d9076b6.jpg",
            amazon_link="",
            category=5
        )
        u2.products.append(p4)

        db.session.commit()

        # Add some example reviews
        r1 = Review(
            content='Good banana',
            rating=4,
            author_id=u1.id
        )
        p1.reviews.append(r1)

        r2 = Review(
            content='Gone off',
            rating=1,
            author_id=u2.id
        )
        p1.reviews.append(r2)

        r3 = Review(
            content='I like lemons',
            rating=4,
            author_id=u1.id
        )
        p4.reviews.append(r3)

        db.session.commit()

        # Add product to wishlist
        u2.wishlist_products.append(p1)
        u2.wishlist_products.append(p2)

        db.session.commit()

        # Generate csrf token in session
        with self.client:
            self.client.get(url_for('login'))
        # Get csrf token
        self.csrf_token = SimpleForm().csrf_token.current_token

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def login(self, email, password):
        return self.client.post('/user/login', data=dict(
            email=email,
            password=password,
            csrf_token=self.csrf_token,
        ), follow_redirects=True)

    def login_u1(self):
        return self.login('u1@u1.com', 'password1')

    def login_u2(self):
        return self.login('u2@u2.com', 'password2')

    def logout(self):
        # Post because of CSRF token
        params = dict(
            csrf_token=self.csrf_token
        )
        return self.client.post('/user/logout', data=params, follow_redirects=True)

    def get_picture_parameter(self):
        tests_dir_path = os.path.dirname(os.path.realpath(__file__))
        with open(os.path.join(tests_dir_path, 'resources', 'street_1.jpg'), 'rb') as pic:
            return (BytesIO(pic.read()), 'pic.jpg')

    def get_text_file_parameter(self):
        tests_dir_path = os.path.dirname(os.path.realpath(__file__))
        with open(os.path.join(tests_dir_path, 'resources', 'text-file.txt'), 'rb') as pic:
            return BytesIO(pic.read()), 'text-file.txt'

    def assertJsonResponse(self, res, msg, status_code):
        json_data = json.loads(res.get_data(as_text=True))
        self.assertEqual(status_code, json_data['code'])
        self.assertIn(msg, json_data['msg'])

    def assertProductPreview(self, p, res_data):
        # Assert details
        self.assertIn(p.name.encode('utf-8'), res_data)
        self.assertIn(p.description.encode('utf-8'), res_data)
        self.assertIn(p.picture_name.encode('utf-8'), res_data)
        price = '{0:.2f}'.format(p.price)
        self.assertIn(price.encode('utf-8'), res_data)
        product_link_path = f'/product/{p.id}'
        self.assertIn(product_link_path.encode('utf-8'), res_data)

        self.assertIn(p.get_star_rating_html().encode('utf-8'), res_data)

    def assertReview(self, r, res_data, view_product=True):
        # Content, rating and creation date
        self.assertIn(r.content.encode('utf-8'), res_data)
        review_created = r.created.strftime("%B %d, %Y")
        self.assertIn(review_created.encode('utf-8'), res_data)

        self.assertIn(r.get_star_rating_html().encode('utf-8'), res_data)

        # View product
        if view_product:
            product_link_path = f'/product/{r.product_id}'
            self.assertIn(product_link_path.encode('utf-8'), res_data)

        # Author
        self.assertIn(r.author.username.encode('utf-8'), res_data)
        self.assertIn(r.author.picture_name.encode('utf-8'), res_data)
        author_link_path = f'<a href="/user/{r.author_id}">'
        self.assertIn(author_link_path.encode('utf-8'), res_data)
