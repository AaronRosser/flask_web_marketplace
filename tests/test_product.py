from Online_Store import app
from Online_Store.models import Product

import unittest
from tests.base import BaseTestCase


class ProductTestCase(BaseTestCase):
    # New product
    def test_new_product_loads(self):
        with self.client:
            self.login_u1()
            res = self.client.get('/product/new')
            self.assert200(res)
            self.assertIn(b'New Product Listing', res.data)

    def test_new_product_not_logged_in(self):
        with self.client:
            res = self.client.get('/product/new')
            self.assertRedirects(res, 'user/login?next=%2Fproduct%2Fnew')

    def test_new_product_correct(self):
        with self.client:
            self.login_u1()
            params = dict(
                name='unique product 123',
                description='very cool product',
                category=1,
                currency='£ (GBP)',
                price='2.20',
                ebay_link='https://www.ebay.co.uk/itm/product123',
                amazon_link='https://www.amazon.co.uk/product123',
                picture=self.get_picture_parameter(),
                csrf_token=self.csrf_token
            )
            res = self.client.post('/product/new', data=params, content_type='multipart/form-data',
                                   follow_redirects=True)

            self.assert200(res)
            self.assertIn(b'unique product 123', res.data)

            p = Product.query.filter_by(
                name='unique product 123'
            ).first()
            self.assertEqual(p.name, params['name'])
            self.assertEqual(p.description, params['description'])
            self.assertEqual(p.category, params['category'])
            self.assertEqual(p.currency, params['currency'])
            self.assertEqual(p.price, 2.2)
            self.assertEqual(p.ebay_link, params['ebay_link'])
            self.assertEqual(p.amazon_link, params['amazon_link'])
            self.assertIsNotNone(p.picture_name)

    def test_new_product_incorrect(self):
        with self.client:
            self.login_u1()
            params = dict(
                name='unique product 123 really long name',
                description='small des',
                category=999,
                currency='not a valid currency',
                price='2.2058',
                ebay_link='https://www.not-ebay.co.uk/product123',
                amazon_link='https://www.fake-amazon.co.uk/product123',
                picture='no picture',
                csrf_token=self.csrf_token
            )
            res = self.client.post('/product/new', data=params, content_type='multipart/form-data',
                                   follow_redirects=True)

            self.assert400(res)
            self.assertIn(b'Field must be between 5 and 25 characters long', res.data)
            self.assertIn(b'Field must be between 10 and 250 characters long', res.data)
            self.assertIn(b'\'999\' is not a valid choice for this field', res.data)
            self.assertIn(b'Invalid format. Price should be in the format 1.99', res.data)
            self.assertIn(b'Not a valid choice', res.data)
            self.assertIn(b'Ebay link not valid', res.data)
            self.assertIn(b'Amazon link not valid', res.data)
            self.assertIn(b'Product picture is required', res.data)

            p = Product.query.filter_by(
                name='unique product 123 really long name'
            ).first()
            self.assertIsNone(p)

    # Edit product
    def test_edit_product_seller(self):
        with self.client:
            self.login_u1()
            res = self.client.get('/product/1/edit')
            self.assert200(res)
            self.assertIn(b'Edit Product Listing', res.data)

    def test_edit_product_wrong_user(self):
        with self.client:
            self.login_u2()
            res = self.client.get('/product/1/edit', follow_redirects=True)
            self.assertIn(b'Product with id [1] does not belong to you', res.data)

    def test_edit_product_not_logged_in(self):
        with self.client:
            res = self.client.get('/product/1/edit')
            self.assertRedirects(res, 'user/login?next=%2Fproduct%2F1%2Fedit')

    def test_edit_product_invalid(self):
        with self.client:
            self.login_u1()
            res = self.client.get('/product/999/edit', follow_redirects=True)
            self.assertIn(b'Product with id [999] not found', res.data)

    def test_edit_product_correct_post(self):
        with self.client:
            original_picture_name = Product.query.get(1).picture_name
            self.login_u1()
            params = dict(
                name='unique product 123',
                description='very cool product',
                category=1,
                currency='£ (GBP)',
                price='2.20',
                ebay_link='https://www.ebay.co.uk/itm/product123',
                amazon_link='https://www.amazon.co.uk/product123',
                picture=self.get_picture_parameter(),
                csrf_token=self.csrf_token
            )
            res = self.client.post('/product/1/edit', data=params,
                                   content_type='multipart/form-data', follow_redirects=True)

            self.assert200(res)
            self.assertIn(b'unique product 123', res.data)

            p = Product.query.get(1)
            self.assertEqual(p.name, params['name'])
            self.assertEqual(p.description, params['description'])
            self.assertEqual(p.category, params['category'])
            self.assertEqual(p.currency, params['currency'])
            self.assertEqual(p.price, 2.2)
            self.assertEqual(p.ebay_link, params['ebay_link'])
            self.assertEqual(p.amazon_link, params['amazon_link'])
            self.assertNotEqual(p.picture_name, original_picture_name)

    def test_edit_product_incorrect(self):
        with self.client:
            self.login_u1()
            params = dict(
                name='unique product 123 really long name',
                description='small des',
                category=999,
                currency='not a valid currency',
                price='2.2058',
                ebay_link='https://www.not-ebay.co.uk/product123',
                amazon_link='https://www.fake-amazon.co.uk/product123',
                picture=self.get_text_file_parameter(),
                csrf_token=self.csrf_token
            )
            res = self.client.post('/product/1/edit', data=params, content_type='multipart/form-data',
                                   follow_redirects=True)

            self.assert400(res)
            self.assertIn(b'Field must be between 5 and 25 characters long', res.data)
            self.assertIn(b'Field must be between 10 and 250 characters long', res.data)
            self.assertIn(b'\'999\' is not a valid choice for this field', res.data)
            self.assertIn(b'Invalid format. Price should be in the format 1.99', res.data)
            self.assertIn(b'Not a valid choice', res.data)
            self.assertIn(b'Ebay link not valid', res.data)
            self.assertIn(b'Amazon link not valid', res.data)
            self.assertIn(b'File does not have an approved extension: jpg, png', res.data)

            p = Product.query.filter_by(
                name='unique product 123 really long name'
            ).first()
            self.assertIsNone(p)

    # Delete product
    def test_delete_product_logged_in_seller(self):
        with self.client:
            self.login_u1()
            params = dict(
                id=1,
                csrf_token=self.csrf_token
            )
            res = self.client.post('/product/delete', data=params, follow_redirects=True)

            self.assert200(res)
            self.assertIn(b'Product deletion succeeded', res.data)

            p = Product.query.get(1)
            self.assertIsNone(p)

    def test_delete_product_logged_in_not_seller(self):
        with self.client:
            self.login_u2()
            params = dict(
                id=1,
                csrf_token=self.csrf_token
            )
            res = self.client.post('/product/delete', data=params, follow_redirects=True)
            self.assertIn(b'Product with id [1] does not belong to you', res.data)

            # Check product was not deleted
            p = Product.query.get(1)
            self.assertEqual(p.name, 'Banana')

    def test_delete_product_not_logged_in(self):
        with self.client:
            params = dict(
                id=1,
                csrf_token=self.csrf_token
            )
            res = self.client.post('/product/delete', data=params)
            self.assertRedirects(res, 'user/login?next=%2Fproduct%2Fdelete')

            # Check product was not deleted
            p = Product.query.get(1)
            self.assertEqual(p.name, 'Banana')

    def test_delete_product_invalid(self):
        self.login_u1()
        with self.client:
            params = dict(
                id=999,
                csrf_token=self.csrf_token
            )
            res = self.client.post('/product/delete', data=params, follow_redirects=True)
            self.assertIn(b'Product with id [999] not found', res.data)

    def test_delete_product_csrf_fail(self):
        self.login_u1()
        with self.client:
            params = dict(
                id=1
            )
            res = self.client.post('/product/delete', data=params, follow_redirects=True)
            self.assertIn(b'Product deletion failed', res.data)

    # View product
    def assertProductPage(self, p, res_data):
        # Product details
        self.assertIn(p.name.encode('utf-8'), res_data)
        self.assertIn(p.description.encode('utf-8'), res_data)
        self.assertIn(p.picture_name.encode('utf-8'), res_data)
        price = '{0:.2f}'.format(p.price)
        self.assertIn(price.encode('utf-8'), res_data)

        self.assertIn(p.get_star_rating_html().encode('utf-8'), res_data)

        # Dates
        created = p.created.strftime("%B %d, %Y")
        self.assertIn(created.encode('utf-8'), res_data)
        last_updated = p.last_updated.strftime("%B %d, %Y")
        self.assertIn(last_updated.encode('utf-8'), res_data)

        # Buy on
        if p.ebay_link:
            self.assertIn(p.ebay_link.encode('utf-8'), res_data)
        if p.amazon_link:
            self.assertIn(p.amazon_link.encode('utf-8'), res_data)

        # Sold by
        self.assertIn(p.seller.username.encode('utf-8'), res_data)
        seller_link = f'/user/{p.seller_id}'
        self.assertIn(seller_link.encode('utf-8'), res_data)

    def test_product_page_loads(self):
        with self.client:
            p = Product.query.get(1)
            res = self.client.get('/product/1')
            self.assert200(res)
            self.assertProductPage(p, res.data)

    def test_product_page_logged_in_seller(self):
        with self.client:
            self.login_u1()
            res = self.client.get('/product/1')
            self.assert200(res)
            self.assertIn(b'>Edit product</a>', res.data)
            self.assertIn(b'/product/1/edit', res.data)
            self.assertIn(b'data-target="#deleteProductModal">Delete Product</button>', res.data)

    def test_product_page_logged_in_not_seller(self):
        with self.client:
            self.login_u2()
            res = self.client.get('/product/1')
            self.assert200(res)
            self.assertNotIn(b'>Edit product</a>', res.data)
            self.assertNotIn(b'/product/1/edit', res.data)
            self.assertNotIn(b'data-target="#deleteProductModal">Delete Product</button>', res.data)

    def test_product_page_not_logged_in(self):
        with self.client:
            res = self.client.get('/product/1')
            self.assert200(res)
            self.assertNotIn(b'>Edit product</a>', res.data)
            self.assertNotIn(b'/product/1/edit', res.data)
            self.assertNotIn(b'data-target="#deleteProductModal">Delete Product</button>', res.data)

    def test_product_page_invalid_product(self):
        with self.client:
            res = self.client.get('/product/999', follow_redirects=True)
            self.assertIn(b'Product with id [999] not found', res.data)

    def test_product_page_post_review_logged_in(self):
        with self.client:
            self.login_u1()
            res = self.client.get('/product/1')
            self.assert200(res)
            self.assertIn(b'<form method="post" action="/product/1/review/post">', res.data)

    def test_product_page_post_review_not_logged_in(self):
        with self.client:
            res = self.client.get('/product/1')
            self.assert200(res)
            self.assertNotIn(b'<form method="post" action="/product/1/review/post">', res.data)

    def test_product_page_reviews_show(self):
        with self.client:
            res = self.client.get('/product/1')
            self.assert200(res)

            p = Product.query.get(1)
            for r in p.reviews:
                self.assertReview(r, res.data, view_product=False)

    def test_product_page_simular_products(self):
        with self.client:
            res = self.client.get('/product/1')
            self.assert200(res)

            product = Product.query.get(1)
            similar_products = product.get_related_products(3)

            for p in similar_products:
                self.assertProductPreview(p, res.data)


if __name__ == '__main__':
    unittest.main()
