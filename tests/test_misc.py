from Online_Store import db
from Online_Store.models import Product

import unittest
from tests.base import BaseTestCase


class MiscTestCase(BaseTestCase):
    def test_home(self):
        with self.client:
            res = self.client.get('/')
            self.assert200(res)
            self.assertIn(b'Home', res.data)

            # Check all products are displayed correctly
            products = Product.query.all()
            for p in products:
                self.assertProductPreview(p, res.data)

    def test_home_no_products(self):
        with self.client:
            # Delete all products
            products = Product.query.all()
            for p in products:
                db.session.delete(p)
            db.session.commit()

            res = self.client.get('/')
            self.assert200(res)
            self.assertIn(b'No products found', res.data)

    def test_404(self):
        with self.client:
            res = self.client.get('/random-page')
            self.assert404(res)
            self.assertIn(b'404 Page not found', res.data)


if __name__ == '__main__':
    unittest.main()
