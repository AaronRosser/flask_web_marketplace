import unittest

from tests.test_user import UserTestCase
from tests.test_product import ProductTestCase
from tests.test_review import ReviewTestCase
from tests.test_wishlist import WishlistTestCase
from tests.test_misc import MiscTestCase

if __name__ == '__main__':
    unittest.main()
