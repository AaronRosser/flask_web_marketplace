# Flask_Web_Marketplace

[Coverage Report](https://aaronrosser.gitlab.io/flask_web_marketplace/)

## Linting / Testing
Activate the python virtual environment
`.\venv\Scripts\Activate.ps1` in powershell (assuming virtual environment in venv folder within project directory)

To run linting run `flake8`

To run unit tests run `pytest`

## Writing tests
All tests should be within the appropriate folder within the `Tests` folder

Each view / endpoint should have a test file named `test_***.py` which should inherit from a base test file for each section

Every test should test one part of each view / endpoints functionality
