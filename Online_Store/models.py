from flask import Markup
from flask_login import UserMixin, current_user

from Online_Store import db, login_manager, bcrypt

from datetime import datetime
from math import floor
from sqlalchemy.sql import func


@login_manager.user_loader
def load_user(user_id):
    return User.query.filter(User.id == int(user_id)).first()


# Create many to many relationship join table
wishlists = db.Table('wishlists',
                     db.Column('user_id', db.Integer, db.ForeignKey('user.id'), nullable=False),
                     db.Column('product_id', db.Integer, db.ForeignKey('product.id'), nullable=False)
                     )


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(40), nullable=False)
    username = db.Column(db.String(25), nullable=False, unique=True)
    email = db.Column(db.String(320), nullable=False, unique=True)
    password_hash = db.Column(db.String(60), nullable=False)
    picture_name = db.Column(db.String(35), nullable=False)

    registered = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)
    last_login = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)

    # Relationships
    products = db.relationship('Product', backref='seller')
    reviews = db.relationship('Review', backref='author')

    wishlist_products = db.relationship('Product', secondary=wishlists,
                                        backref=db.backref('wishlist_users', lazy=True))

    def rel_profile_picture_path(self):
        return 'profile_pictures/' + self.picture_name

    def set_password(self, password):
        self.password_hash = bcrypt.generate_password_hash(password).decode('utf-8')


class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(25), nullable=False)
    description = db.Column(db.String(250), nullable=False)
    picture_name = db.Column(db.String(35), nullable=False)
    category = db.Column(db.SmallInteger, nullable=False)

    currency = db.Column(db.String(5), nullable=False)
    price = db.Column(db.Float, nullable=False)

    ebay_link = db.Column(db.String(150), nullable=True)
    amazon_link = db.Column(db.String(150), nullable=True)

    created = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)
    last_updated = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)

    # Relationships
    seller_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    reviews = db.relationship('Review', backref='product', cascade="all, delete")

    def rel_picture_path(self):
        return 'product_pictures/' + self.picture_name

    def get_average_rating(self):
        avg_rating = db.session.query(func.avg(Review.rating))\
                               .filter(Review.product_id == self.id).scalar()
        if avg_rating is None or avg_rating > 5:
            return 0
        # Round to nearest 0.5
        return round(avg_rating * 2) / 2

    def get_star_rating_html(self):
        avg_rating = self.get_average_rating()
        # Full stars
        stars = '<i class="bi bi-star-fill"></i>' * floor(avg_rating)
        # Half star
        if avg_rating != floor(avg_rating):
            stars += '<i class="bi bi-star-half"></i>'
        # Empty stars
        stars += '<i class="bi bi-star"></i>' * floor(5 - avg_rating)
        return Markup(stars)

    def get_price(self):
        return self.currency[0] + '{0:.2f}'.format(self.price) + self.currency[1:]

    def get_related_products(self, limit):
        return Product.query.filter(
            Product.category == self.category,
            Product.id != self.id
        ).limit(limit).all()


class Review(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    content = db.Column(db.String(120), nullable=False)
    rating = db.Column(db.SmallInteger, nullable=False)

    created = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)

    # Relationships
    author_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'), nullable=False)

    def get_star_rating_html(self):
        stars = '<i class="bi bi-star-fill"></i>' * self.rating
        stars += '<i class="bi bi-star"></i>' * (5 - self.rating)
        return Markup(stars)

    def can_delete(self):
        if not current_user.is_authenticated:
            return False

        if current_user.id != self.author_id:
            return False

        return True
