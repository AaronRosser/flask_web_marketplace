from flask import render_template
from flask_api import status

from Online_Store import app, db
from Online_Store.models import Product


@app.route('/', methods=['GET', 'POST'])
def home():
    products = db.session.query(Product).all()
    return render_template("home.html", title='Home', products=products)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('errors/404.html'), status.HTTP_404_NOT_FOUND
