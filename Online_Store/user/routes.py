from flask import render_template, flash, redirect, url_for, request
from flask_api import status
from flask_login import login_user, current_user, logout_user, login_required

from Online_Store import app, db, bcrypt
from Online_Store.user.forms import RegistrationForm, LoginForm, ChangeDetailsForm, ChangePasswordForm
from Online_Store.forms import SimpleForm
from Online_Store.models import User
from Online_Store.helper_functions import save_picture, get_redirect_url

from datetime import datetime
from is_safe_url import is_safe_url


@app.route('/user/register', methods=['GET', 'POST'])
def register():
    # Check if user logged in
    if current_user.is_authenticated:
        return redirect(url_for('home'))

    form = RegistrationForm()
    if request.method == 'GET':
        return render_template('user/register.html', title='Register', form=form)

    # Validate form submission
    if not form.validate_on_submit():
        return render_template('user/register.html', title='Register', form=form), status.HTTP_400_BAD_REQUEST

    # Registration successful
    new_user = User(
        name=form.name.data,
        username=form.username.data,
        email=form.email.data,
        picture_name=save_picture(form.picture.data, 'profile_pictures')
    )
    new_user.set_password(form.password.data)

    # Add user to db
    db.session.add(new_user)
    db.session.commit()

    # Show success alert
    flash(f'User [ {form.username.data} ] was created successfully.', 'success')
    return redirect(url_for('login'))


@app.route('/user/login', methods=['GET', 'POST'])
def login():
    # Check if user logged in
    if current_user.is_authenticated:
        return redirect(get_redirect_url())

    form = LoginForm()

    if request.method == 'GET':
        return render_template('user/login.html', title='Login', form=form)

    # Validate form submission
    if not form.validate_on_submit():
        return render_template('user/login.html', title='Login', form=form), status.HTTP_400_BAD_REQUEST

    # Check for user
    u = User.query.filter_by(email=form.email.data).first()
    if not u or not bcrypt.check_password_hash(u.password_hash, form.password.data):
        flash('Login failed. Provided details were incorrect.', 'danger')
        return render_template('user/login.html', title='Login', form=form), status.HTTP_400_BAD_REQUEST

    # Login successful
    login_user(u, form.remember.data)
    flash('Login successful', 'success')

    # Update last login date time
    u.lastLogin = datetime.now()
    db.session.commit()

    # Redirect if user was trying to access restricted page
    redirect_page = request.args.get('next')
    if redirect_page and is_safe_url(redirect_page, app.config['SAFE_URL_HOSTS']):
        return redirect(redirect_page)

    return redirect(url_for('home'))


@app.route('/user/logout', methods=['POST'])
def logout():
    if not current_user.is_authenticated:
        flash('Logout failed. No user logged in', 'danger')
        return redirect(get_redirect_url())

    form = SimpleForm()
    del form.id
    if not form.validate_on_submit():
        flash('Logout failed', 'danger')
        return redirect(url_for('home'))

    logout_user()
    flash('Logout successful', 'success')
    return redirect(url_for('home'))


@app.route('/user/<int:user_id>')
def user(user_id):
    u = User.query.get(user_id)
    if not u:
        flash(f'User with id [{user_id}] not found', 'danger')
        return redirect(get_redirect_url())
    return render_template('user/user.html', title=f'{u.username}\'s Profile', user=u)


def render_manage_user(change_details_form=None, change_password_form=None, res_status=status.HTTP_200_OK):
    if change_details_form is None:
        change_details_form = ChangeDetailsForm()
        # Initialize form
        change_details_form.name.data = current_user.name
        change_details_form.username.data = current_user.username
        change_details_form.email.data = current_user.email
    if change_password_form is None:
        change_password_form = ChangePasswordForm()

    return render_template('user/manage_user.html', title='Manage Account', details_form=change_details_form,
                           password_form=change_password_form), res_status


@app.route('/user/manage')
@login_required
def manage_user():
    return render_manage_user()


@app.route('/user/manage/update_user_details', methods=['POST'])
@login_required
def update_user_details():
    form = ChangeDetailsForm()

    # Validate submitted details
    if not form.validate_on_submit():
        return render_manage_user(change_details_form=form, res_status=status.HTTP_400_BAD_REQUEST)

    current_user.name = form.name.data
    current_user.username = form.username.data
    current_user.email = form.email.data

    if form.picture.data:
        current_user.picture_name = save_picture(form.picture.data, 'profile_pictures')

    db.session.commit()
    flash('Details successfully updated', 'success')

    return render_manage_user(change_details_form=form)


@app.route('/user/manage/change_password', methods=['POST'])
@login_required
def change_password():
    form = ChangePasswordForm()

    if not form.validate_on_submit():
        return render_manage_user(change_password_form=form, res_status=status.HTTP_400_BAD_REQUEST)

    # Update password
    current_user.set_password(form.password.data)
    db.session.commit()
    flash('Password updated successfully', 'success')
    return render_manage_user(change_password_form=form)


@app.route('/user/<int:user_id>/listings', methods=['GET'])
def listings(user_id):
    # Check user exists
    u = User.query.get(user_id)
    if not u:
        flash(f'User with id [{user_id}] not found', 'danger')
        return redirect(url_for('home'))

    return render_template("user/listings.html", title=f'{u.username}\'s listings', user=u)


@app.route('/user/<int:user_id>/reviews', methods=['GET', 'POST'])
def reviews(user_id):
    # Check user exists
    u = User.query.get(user_id)
    if not u:
        flash(f'User with id [{user_id}] not found', 'danger')
        return redirect(url_for('home'))

    return render_template("user/reviews.html", title=f'{u.username}\'s reviews', user=u)
