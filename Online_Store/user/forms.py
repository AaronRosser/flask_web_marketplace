from flask_wtf import FlaskForm
from flask_login import current_user
from flask_wtf.file import FileField, FileAllowed

from wtforms import StringField, PasswordField, SubmitField, BooleanField
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError

from Online_Store import bcrypt
from Online_Store.models import User


class RegistrationForm(FlaskForm):
    name = StringField('Name *', validators=[DataRequired(), Length(min=2, max=40)])
    username = StringField('Username *', validators=[DataRequired(), Length(min=2, max=25)])
    email = EmailField('Email *', validators=[DataRequired(), Length(max=320), Email()])
    picture = FileField('Upload product picture *', validators=[DataRequired(message='Profile picture is required.'),
                                                                FileAllowed(['jpg', 'jpeg', 'png'])])

    password = PasswordField('Password *', validators=[DataRequired()])
    password_confirm = PasswordField('Confirm Password *',
                                     validators=[DataRequired(), EqualTo('password')])

    submit = SubmitField('Sign Up')

    # Check email is not already taken
    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if not user:
            return
        raise ValidationError('Email already registered to an account')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if not user:
            return
        raise ValidationError('Username already registered to an account')


class LoginForm(FlaskForm):
    email = EmailField('Email', validators=[DataRequired(), Length(max=320), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember Me')

    submit = SubmitField('Login')


class ChangePasswordForm(FlaskForm):
    current_password = PasswordField('Current Password', validators=[DataRequired()])

    password = PasswordField('New Password', validators=[DataRequired()])
    password_confirm = PasswordField('Confirm New Password',
                                     validators=[DataRequired(), EqualTo('password')])

    submit = SubmitField('Save')

    def validate_current_password(self, current_password):
        if bcrypt.check_password_hash(current_user.password_hash, current_password.data):
            return

        raise ValidationError('Incorrect password')


class ChangeDetailsForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired(), Length(min=2, max=25)])
    username = StringField('Username', validators=[DataRequired(), Length(min=2, max=25)])
    email = EmailField('Email', validators=[DataRequired(), Length(max=320), Email()])
    picture = FileField('Upload product picture *', validators=[FileAllowed(['jpg', 'png'])])

    submit = SubmitField('Save')

    # Check email is not already taken by another user
    def validate_email(self, email):
        if email.data == current_user.email:
            return

        user = User.query.filter_by(email=email.data).first()
        if not user:
            return
        raise ValidationError('Email already registered to an account')

    # Check username is not already taken by another user
    def validate_username(self, username):
        if username.data == current_user.username:
            return

        user = User.query.filter_by(username=username.data).first()
        if not user:
            return
        raise ValidationError('Username already registered to an account')
