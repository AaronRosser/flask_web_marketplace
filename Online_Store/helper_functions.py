from flask import current_app, Markup, request, url_for, jsonify

from Online_Store import app

import os
import secrets
from is_safe_url import is_safe_url
from PIL import Image, ImageOps


def save_picture(picture, static_folder):
    extension = os.path.splitext(picture.filename)[-1]
    name = secrets.token_hex(12) + extension
    path = os.path.join(current_app.root_path, 'static', static_folder, name)

    output_size = (500, 500)
    pic = Image.open(picture)
    # Fix image orientation
    pic = ImageOps.exif_transpose(pic)

    pic.thumbnail(output_size)
    pic.save(path)

    return name


def get_field_class(errors):
    if errors:
        return 'form-control is-invalid'
    return 'form-control'


def format_errors(errors):
    html = '<div class="invalid-feedback">'
    for err in errors:
        html += f'<p class="mb-0">{err}</p>'
    html += '</div>'
    return Markup(html)


def get_redirect_url():
    url = request.args.get('next') or request.referrer
    if url and is_safe_url(url, app.config['SAFE_URL_HOSTS']):
        return url
    return url_for('home')


def get_json_response(message, status_code):
    response = {
        'code': status_code,
        'msg': message
    }
    return jsonify(response), status_code
