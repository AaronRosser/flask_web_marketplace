from flask import flash, redirect, url_for, render_template, request, jsonify
from flask_api import status
from flask_login import login_required, current_user

from Online_Store import app, db
from Online_Store.models import Product, Review
from Online_Store.review.forms import NewReviewForm
from Online_Store.forms import SimpleForm
from Online_Store.product.routes import render_product
from Online_Store.helper_functions import get_json_response, get_redirect_url


@app.route('/product/<int:product_id>/review/post', methods=['POST'])
@login_required
def post_review(product_id):
    # Get product
    product = Product.query.get(product_id)
    if not product:
        flash(f'Product with id [{product_id}] not found.', 'danger')
        return redirect(get_redirect_url())

    form = NewReviewForm()
    if not form.validate_on_submit():
        return render_product(product_id, new_review_form=form, status_code=status.HTTP_400_BAD_REQUEST)

    r = Review(
        content=form.content.data,
        rating=form.rating.data,
        author_id=current_user.id,
        product_id=product_id
    )
    db.session.add(r)
    db.session.commit()
    return redirect(url_for('product', product_id=product_id))


@app.route('/review/delete', methods=['POST'])
def delete_review():
    # Require login
    if not current_user.is_authenticated:
        return get_json_response('Unauthorized.', status.HTTP_401_UNAUTHORIZED)

    form = SimpleForm()
    if not form.validate_on_submit():
        return get_json_response('Missing review id', status.HTTP_400_BAD_REQUEST)

    r = Review.query.get(form.id.data)
    if not r:
        return get_json_response(f'Review with id [{form.id.data}] not found', status.HTTP_400_BAD_REQUEST)

    if r.author_id != current_user.id:
        return get_json_response(f'Review with id [{form.id.data}] does not belong to current user',
                                 status.HTTP_400_BAD_REQUEST)

    db.session.delete(r)
    db.session.commit()

    return get_json_response(f'Successfully deleted review with id [{form.id.data}]', status.HTTP_200_OK)
