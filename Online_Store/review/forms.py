from flask_wtf import FlaskForm

from wtforms import TextAreaField, SubmitField, IntegerField
from wtforms.validators import DataRequired, Length, NumberRange


class NewReviewForm(FlaskForm):
    content = TextAreaField('Review', validators=[DataRequired(), Length(min=10, max=120)])
    rating = IntegerField('', validators=[DataRequired(),
                                          NumberRange(min=1, max=5, message='Ratings must be between 1 and 5 stars')])

    submit = SubmitField('Post')
