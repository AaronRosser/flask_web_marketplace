from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed

from wtforms import StringField, SubmitField, TextAreaField, SelectField, SelectMultipleField
from wtforms.fields.html5 import DecimalField, URLField
from wtforms.validators import DataRequired, Length, ValidationError, NumberRange

from Online_Store.models import Product
from Online_Store.helper_functions import save_picture

import re
from datetime import datetime

categories = [
    ('1', 'Electronics'),
    ('2', 'Home / Garden'),
    ('3', 'Fashion'),
    ('4', 'Business / Office, Industrial'),
    ('5', 'Food'),
    ('6', 'Other')
]
currencies = [
    '£ (GBP)',
    '€ (EUR)',
    '$ (USD)',
    '$ (AUD)',
    '¥ (JPY)']
price_pattern = re.compile(r'^([0-9]+)(\.)([0-9]{2})$')
ebay_pattern = re.compile(r'^(https://)(www|)(\.ebay\.co\.uk/itm/)(.+)$')
amazon_pattern = re.compile(r'^(https://)(www|)(\.amazon\.)(co\.uk|com)(/.+)$')


class NewProductForm(FlaskForm):
    name = StringField('Product Name *', validators=[DataRequired(), Length(min=5, max=25)])
    description = TextAreaField('Description *', validators=[DataRequired(), Length(min=10, max=250)])
    picture = FileField('Upload product picture *',
                        validators=[DataRequired(message='Product picture is required'),
                                    FileAllowed(['jpg', 'jpeg', 'png'])])
    category = SelectMultipleField('Category *', choices=categories, validators=[DataRequired()])

    currency = SelectField('Currency *', choices=currencies, validators=[DataRequired()])
    price = DecimalField('Price *', validators=[DataRequired(), NumberRange(min=0)])

    ebay_link = URLField('Ebay Link (https://ebay.co.uk/item/...)', validators=[Length(max=150)],
                         filters=[lambda x: x or None])
    amazon_link = URLField('Amazon Link (https://amazon.co.uk/...)', validators=[Length(max=150)],
                           filters=[lambda x: x or None])

    submit = SubmitField('Add')

    # Ensure price is correct format
    def validate_price(self, price):
        if price_pattern.match(str(price.data)):
            return

        raise ValidationError('Invalid format. Price should be in the format 1.99')

    # Ensure link is for ebay.co.uk
    def validate_ebay_link(self, link):
        if link.data is None or ebay_pattern.match(link.data):
            return

        raise ValidationError('Ebay link not valid')

    # Ensure link is for amazon.co.uk
    def validate_amazon_link(self, link):
        if not link.data or amazon_pattern.match(link.data):
            return

        raise ValidationError('Amazon link not valid')

    def get_product(self, p=None):
        if p is None:
            p = Product()
        p.name = self.name.data
        p.description = self.description.data
        p.category = self.category.data[0]
        p.currency = self.currency.data
        p.price = self.price.data
        p.ebay_link = self.ebay_link.data
        p.amazon_link = self.amazon_link.data

        p.last_updated = datetime.now()
        if self.picture.data:
            p.picture_name = save_picture(self.picture.data, 'product_pictures')
        return p


class EditProductForm(NewProductForm):
    picture = FileField('Upload product picture *', validators=[FileAllowed(['jpg', 'png'])])
    submit = SubmitField('Update')

    # Initialize form with product model
    def __init__(self, product=None):
        super().__init__()
        if product is None:
            return
        self.name.default = product.name
        self.description.default = product.description
        self.category.default = [product.category]
        self.currency.default = product.currency
        self.price.default = product.price
        self.ebay_link.default = product.ebay_link
        self.amazon_link.default = product.amazon_link
        self.process()
