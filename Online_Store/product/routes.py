from flask import render_template, flash, redirect, url_for, request
from flask_login import login_required, current_user
from flask_api import status

from Online_Store import app, db
from Online_Store.product.forms import NewProductForm, EditProductForm, categories
from Online_Store.review.forms import NewReviewForm
from Online_Store.forms import SimpleForm
from Online_Store.models import Product
from Online_Store.helper_functions import get_redirect_url


@app.route('/product/new', methods=['GET', 'POST'])
@login_required
def new_product():
    form = NewProductForm()
    if request.method == 'GET':
        return render_template('product/new_product.html', title='New Product', form=form)

    # Validate submitted form
    if not form.validate_on_submit():
        return render_template('product/new_product.html', title='New Product', form=form), status.HTTP_400_BAD_REQUEST

    p = form.get_product()

    current_user.products.append(p)
    db.session.commit()

    # Change to product page
    return redirect(url_for('product', product_id=p.id))


@app.route('/product/<int:product_id>/edit', methods=['GET', 'POST'])
@login_required
def edit_product(product_id):
    # Get product
    p = Product.query.get(product_id)
    if not p:
        flash(f'Product with id [{product_id}] not found.', 'danger')
        return redirect(get_redirect_url())

    # Check user owns product
    if current_user.id != p.seller_id:
        flash(f'Product with id [{product_id}] does not belong to you.', 'danger')
        return redirect(get_redirect_url())

    if request.method == 'GET':
        form = EditProductForm(p)
        return render_template('product/edit_product.html', title='Edit Product', form=form, product=p)

    form = EditProductForm()
    if not form.validate_on_submit():
        return render_template('product/edit_product.html', title='Edit Product', form=form, product=p), \
            status.HTTP_400_BAD_REQUEST

    form.get_product(p)
    db.session.commit()

    # Change to product page
    return redirect(url_for('product', product_id=product_id))


@app.route('/product/delete', methods=['POST'])
@login_required
def delete_product():
    form = SimpleForm()
    if not form.validate_on_submit():
        flash('Product deletion failed.', 'danger')
        return redirect(get_redirect_url())

    # Get product
    p = Product.query.get(form.id.data)
    if not p:
        flash(f'Product with id [{form.id.data}] not found.', 'danger')
        return redirect(get_redirect_url())

    # Check user owns product
    if current_user.id != p.seller_id:
        flash(f'Product with id [{form.id.data}] does not belong to you.', 'danger')
        return redirect(get_redirect_url())

    db.session.delete(p)
    db.session.commit()
    flash('Product deletion succeeded.', 'success')
    return redirect(url_for('home'))


def render_product(product_id, new_review_form=None, status_code=status.HTTP_200_OK):
    # Get product
    p = Product.query.get(product_id)
    if not p:
        flash(f'Product with id [{product_id}] not found', 'danger')
        return redirect(get_redirect_url())

    if new_review_form is None:
        new_review_form = NewReviewForm()

    category_name = dict(categories)[str(p.category)]
    related_products = p.get_related_products(3)
    return render_template('product/product.html',
                           title=p.name,
                           product=p,
                           new_review_form=new_review_form,
                           category=category_name,
                           related_products=related_products,
                           no_view_product=True), status_code


@app.route('/product/<int:product_id>', methods=['GET'])
def product(product_id):
    return render_product(product_id)
