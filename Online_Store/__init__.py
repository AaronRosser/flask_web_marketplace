from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager


app = Flask(__name__)
# Load config
if app.config['ENV'] == 'devolpment':
    app.config.from_object('config.DevolpmentConfig')
elif app.config['ENV'] == 'testing':
    app.config.from_object('config.TestingConfig')
else:
    app.config.from_object('config.ProductionConfig')

db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = 'login'
login_manager.login_message_category = 'danger'

# Add custom functions to all templates
from Online_Store.helper_functions import get_field_class, format_errors
app.jinja_env.globals.update(get_field_class=get_field_class)
app.jinja_env.globals.update(format_errors=format_errors)

# Add simple form to all templates
from Online_Store.forms import SimpleForm


@app.context_processor
def inject_simple_form():
    return dict(simple_form=SimpleForm())


from Online_Store import routes
from Online_Store.user import routes as user_roues
from Online_Store.product import routes as product_routes
from Online_Store.wishlist import routes as wishlist_routes
from Online_Store.review import routes as review_roues
