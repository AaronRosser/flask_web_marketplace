from flask import flash, redirect, url_for, render_template, jsonify
from flask_login import login_required, current_user
from flask_api import status

from Online_Store import app, db
from Online_Store.models import Product
from Online_Store.forms import SimpleForm
from Online_Store.helper_functions import get_json_response


@app.route('/wishlist/add', methods=['POST'])
def wishlist_product_add():
    # Require login
    if not current_user.is_authenticated:
        return get_json_response('Unauthorized.', status.HTTP_401_UNAUTHORIZED)

    form = SimpleForm()
    if not form.validate_on_submit():
        return get_json_response('Invalid or missing product id.', status.HTTP_400_BAD_REQUEST)

    # Get product
    product = Product.query.get(form.id.data)
    if not product:
        return get_json_response('Product not found.', status.HTTP_400_BAD_REQUEST)

    if product in current_user.wishlist_products:
        return get_json_response('Product already in wishlist.', status.HTTP_400_BAD_REQUEST)

    current_user.wishlist_products.append(product)
    db.session.commit()

    return get_json_response('Product successfully added to wishlist.', status.HTTP_200_OK)


@app.route('/wishlist/remove', methods=['POST'])
def wishlist_product_remove():
    # Require login
    if not current_user.is_authenticated:
        return get_json_response('Unauthorized.', status.HTTP_401_UNAUTHORIZED)

    form = SimpleForm()
    if not form.validate_on_submit():
        return get_json_response('Invalid or missing product id', status.HTTP_400_BAD_REQUEST)

    # Get product
    product = Product.query.get(form.id.data)
    if not product:
        return get_json_response('Product not found.', status.HTTP_400_BAD_REQUEST)

    if product not in current_user.wishlist_products:
        return get_json_response('Product not in wishlist', status.HTTP_400_BAD_REQUEST)

    current_user.wishlist_products.remove(product)
    db.session.commit()

    return get_json_response('Product successfully removed from wishlist.', status.HTTP_200_OK)


@app.route('/wishlist', methods=['GET'])
@login_required
def wishlist():
    products = db.session.query(Product).filter(
        Product.wishlist_users.any(id=current_user.id)
    ).all()
    return render_template("wishlist/wishlist.html", title='My Wishlist', products=products)
