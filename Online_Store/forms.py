from flask_wtf import FlaskForm

from wtforms import SubmitField, IntegerField
from wtforms.validators import DataRequired, NumberRange


class SimpleForm(FlaskForm):
    id = IntegerField('', validators=[DataRequired(), NumberRange(min=0)])
