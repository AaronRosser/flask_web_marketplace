from Online_Store import db
from Online_Store.models import Product, User

user = User.query.first()

p1 = Product(
    name="Banana",
    description="1 of 5 a day = 1 medium banana and source of vitamin B6",
    price=1.56,
    currency="£ (GBP)",
    ebay_link="",
    picture_name="425c794c15b84d8cd844c960.jpg",
    amazon_link="",
    category=1
)
user.products.append(p1)

p2 = Product(
    name="Apple",
    description="Specially ripened in UK by Fyffes dedicated team, ready to eat",
    price=2.65,
    currency="£ (GBP)",
    ebay_link="https://www.ebay.co.uk/itm/Mini-Fruit-Tree-Patio-Potted-Garden-Plants-Apple-Pear-Plum-Cherry-9cm-Pots-"
              "T-M/372340119783",
    picture_name="e52e5ca03369e9d282bea813.jpg",
    amazon_link="",
    category=1
)
user.products.append(p2)

p3 = Product(
    name="Orange",
    description="100% Fairtrade Organic certified and sourced from Fairtrade producers",
    price=1.2,
    currency="£ (GBP)",
    ebay_link="https://www.ebay.co.uk/itm/Fresh-Seville-Oranges-2021-Season-Fast-Delivery/224309055937",
    picture_name="fd4e4dfb7dea4ed0f99d2b61.jpg",
    amazon_link="",
    category=1
)
user.products.append(p3)

p4 = Product(
    name="Lemon",
    description="AMAZZING brings you the extraordinary tastes of the freshest fruits from the best sources",
    price=2.5,
    currency="£ (GBP)",
    ebay_link="",
    picture_name="23be665d6a00a7ca6d9076b6.jpg",
    amazon_link="",
    category=1
)
user.products.append(p4)

p5 = Product(
    name="Pear",
    description="Country of Origin will vary depending on the season",
    price=1.45,
    currency="£ (GBP)",
    ebay_link="",
    picture_name="70177259bcc37d2a6fcdb1b3.jpg",
    amazon_link="",
    category=1
)
user.products.append(p5)

p6 = Product(
    name="Grape",
    description="Refrigerate for Freshnes",
    price=3.5,
    currency="£ (GBP)",
    ebay_link="",
    picture_name="aaeaae482b71595621de5d85.jpg",
    amazon_link="",
    category=1
)
user.products.append(p6)

db.session.commit()
