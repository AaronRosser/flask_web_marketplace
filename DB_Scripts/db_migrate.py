from Online_Store import app, db
from flask_sqlalchemy import SQLAlchemy
from Online_Store.models import User, Product, Review
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

migrate = Migrate(app, db, render_as_batch=True)
manager = Manager(app)

manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
